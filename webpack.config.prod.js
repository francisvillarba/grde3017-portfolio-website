/**
 * Webpack Configuration for GRDE3017 - Digital Portfolio Design - Assignment 3
 * @author Francis Villarba
 */

// Imports ------------------------------------------------------------------ /

const webpack = require('webpack');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const ObsoleteWebpackPlugin = require('obsolete-webpack-plugin');

// Configuration ------------------------------------------------------------ /

const config = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'js/[name].[contenthash].js'
    },
    resolve: {
        modules: ['node_modules']
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.js$/,
                use: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.html$/i,
                loader: 'html-loader',
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: [
                    'file-loader',
                    {
                        loader: 'image-webpack-loader',
                        options: {
                            bypassOnDebug: false, // webpack@1.x
                            disable: false, // webpack@2.x and newer
                            mozjpeg: {
                                progressive: true,
                                quality: 65,
                            },
                            // optipng.enabled: false will disable optipng
                            optipng: {
                                enabled: false,
                            },
                            pngquant: {
                                quality: [0.65, 0.90],
                                speed: 4
                            },
                            gifsicle: {
                                interlaced: false,
                            },
                        }
                    }
                ],
            },
            {
                test: /\.mp4$/,
                use: 'file-loader?name=videos/[name].[ext]',
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new CopyPlugin({
            patterns: [
                { from: path.resolve(__dirname, 'src/views'), to: 'views' },
                { from: path.resolve(__dirname, 'static'), to: 'static'},
            ]
        }),
        new HtmlWebpackPlugin({
            inject: true,
            scriptLoading: 'defer',
            title: 'Francis Villarba',
            template: 'src/index.html'
        }),
        new ObsoleteWebpackPlugin({
            promptOnNonTargetBrowser: true,
        }),
        new FaviconsWebpackPlugin({
            logo: './src/logo.png',
            cache: true,
            mode: 'webapp',
            devMode: 'webapp',
            favicons: {
                appName: 'Francis-Villarba-Portfolio-Site',
                appDescription: 'Portfolio Website created by Francis Villarba for himself',
                developerName: 'Francis Villarba <hello@francisvillarba.com>',
                icons: {
                    coast: false,
                    yandex: false
                }
            }
        }),
        new MiniCssExtractPlugin(),
        new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            openAnalyzer: false,
        }),
    ],
    optimization: {
        runtimeChunk: 'single',
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    chunks: 'all'
                }
            }
        }
    },
    devServer: {
        lazy: false,
        liveReload: true,
        contentBase: path.join(__dirname, 'dist'),
        clientLogLevel: 'debug',
        compress: false,
        port: 8080,
    }
};

// Exports ------------------------------------------------------------------ /
module.exports = config;