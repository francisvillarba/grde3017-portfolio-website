/**
 * js/particles.js
 * Simply used to add particles to the hero banner area using particles.js
 * 
 * @author Francis Villarba <hello@francisvillarba.com>
 */

import 'particles.js/particles';
const particlesJS = window.particlesJS;

export function runParticles() {
    particlesJS.load('particles-target', 'static/particles-config.json'), () => {
        console.log(`Welcome to Francis Villarba's Portfolio!`);    
    };
}