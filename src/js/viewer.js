/**
 * js/viewer.js
 * 
 * Project viewer script for my personal portfolio website
 * @author Francis Villarba <hello@francisvillarba.com>
 */

// Imports ------------------------------------------------------------------ /
import UIkit from 'uikit';

// Constants ---------------------------------------------------------------- /
const PROJECTS_TARGET_ELEMENT_ID = '#project-viewer';
const PROJECTS_ELEMENT_CLOSE_BUTTON_ID = '#project-viewer-close';
const PROJECT_VIEWER_FRAME_ID = '#project-frame';
const PROJECT_VIEWER_TITLE_ID = '#project-title';

// Functionality ------------------------------------------------------------ /

function handleCloseViewerEvent() {
    document.querySelector(PROJECTS_ELEMENT_CLOSE_BUTTON_ID).addEventListener('click', () => {
        UIkit.modal(PROJECTS_TARGET_ELEMENT_ID).hide();
        document.querySelector(PROJECT_VIEWER_FRAME_ID).setAttribute('src', "#");
    });
};

export function showProject(title, webpack_data) {
    document.querySelector(PROJECT_VIEWER_TITLE_ID).innerText = title;
    document.querySelector(PROJECT_VIEWER_FRAME_ID).innerHTML = webpack_data;
    document.getElementsByTagName("BODY")[0].style = 'overflow: hidden';
    UIkit.modal(PROJECTS_TARGET_ELEMENT_ID).show();
    // Ensure modal is scrolled to the top when the project is opened
    document.querySelector(PROJECT_VIEWER_FRAME_ID).scrollTop = 0;
    handleCloseViewerEvent();
};