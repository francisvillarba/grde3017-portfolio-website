/**
 * js/projects.js
 * 
 * The projects handler scripts for my personal portfolio project
 * @author Francis Villarba <hello@francisvillarba.com>
 */

// Imports ------------------------------------------------------------------ /

import { showProject } from './viewer';

// Constants ---------------------------------------------------------------- /

const CURTIN_GRADSHOW_PROJECT_PRIMARY_ID = '#project-button-a';
const SARGENT_TOURS_PROJECT_ID = '#project-button-b';
const AERIUS_DESKTOP_PROJECT_ID = '#project-button-c';
const RESTAURANT_SYSTEM_PROJECT_ID = '#project-button-d';
const CURTIN_ENROLMENTS_PROJECT_ID = '#project-button-e';

const CURTIN_GRADSHOW_SECONDARY_ID = '#other-project-button-a';
const PHOTOGRAPHY_ASSIGNMENT_PROJECT_ID = '#other-project-button-b';
const BEYOND_CURTIN_GRADSHOW_PROJECT_ID = '#other-project-button-c';
const SFX_COMMISSION_PROJECT_ID = '#other-project-button-d';
const EVD_COMMERCIAL_PROJECT_ID = '#other-project-button-e';
const BAMBAMBOO_WORK_PROJECT_ID = '#other-project-button-f';

const PROJECT_COMING_SOON_URL = require('../views/coming_soon.html');
const CURTIN_GRADSHOW_PRIMARY_URL = require('../views/curtin_gradshow_primary.html');
const AERIUS_DESKTOP_PROJECT_URL = require('../views/aerius_cloud_desktop.html');
const SARGENT_TOURS_PROJECT_URL = require('../views/sargent_tours.html');
const RESTAURANT_SYSTEM_PROJECT_URL = require('../views/restaurant_system.html');
const CURTIN_GRADSHOW_PHOTO_URL = require('../views/curtin_gradshow_photography.html');
const PHOTOGRAPHY_ASSIGNMENT_URL = require('../views/photo_assignment.html');

// Functionality ------------------------------------------------------------ /

export function initProjectHandlers() {

    document.querySelector(CURTIN_GRADSHOW_PROJECT_PRIMARY_ID).addEventListener('click', () => {
        showProject('Curtin Grad Show', CURTIN_GRADSHOW_PRIMARY_URL);
    });

    document.querySelector(SARGENT_TOURS_PROJECT_ID).addEventListener('click', () => {
        showProject('Sargent Tours', SARGENT_TOURS_PROJECT_URL);
    });

    document.querySelector(AERIUS_DESKTOP_PROJECT_ID).addEventListener('click', () => {
        showProject('Aerius Desktop', AERIUS_DESKTOP_PROJECT_URL);
    });

    document.querySelector(RESTAURANT_SYSTEM_PROJECT_ID).addEventListener('click', () => {
        showProject('Restaurant System', RESTAURANT_SYSTEM_PROJECT_URL);
    });

    // TODO - Replace with real project description
    document.querySelector(CURTIN_ENROLMENTS_PROJECT_ID).addEventListener('click', () => {
        showProject('Curtin Enrolments', PROJECT_COMING_SOON_URL);
    });

    document.querySelector(CURTIN_GRADSHOW_SECONDARY_ID).addEventListener('click', () => {
        showProject('Curtin Grad Show', CURTIN_GRADSHOW_PHOTO_URL);
    });

    document.querySelector(PHOTOGRAPHY_ASSIGNMENT_PROJECT_ID).addEventListener('click', () => {
        showProject('Photography Assignment', PROJECT_COMING_SOON_URL);
    });

    // TODO - Replace with real project description
    document.querySelector(BEYOND_CURTIN_GRADSHOW_PROJECT_ID).addEventListener('click', () => {
        showProject('Beyond Curtin Grad Show Photography', PROJECT_COMING_SOON_URL);
    });

    // TODO - Replace with real project description
    document.querySelector(SFX_COMMISSION_PROJECT_ID).addEventListener('click', () => {
        showProject('Sound Effects Commission', PROJECT_COMING_SOON_URL);
    });

    // TODO - Replace with real project description
    document.querySelector(EVD_COMMERCIAL_PROJECT_ID).addEventListener('click', () => {
        showProject('EVD Commercial', PROJECT_COMING_SOON_URL);
    });

    // TODO - Replace with real project description
    document.querySelector(BAMBAMBOO_WORK_PROJECT_ID).addEventListener('click', () => {
        showProject('BamBamBoo Media', PROJECT_COMING_SOON_URL);
    });
}