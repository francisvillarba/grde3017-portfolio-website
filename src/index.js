/**
 * The main Webpack / Javascript entrypoint for the
 * DPD 3017 - Portfolio Website
 * @author Francis Villarba <hello@francisvillarba.com)
 */

// Webpack specific imports ------------------------------------------------- /

// For the uikit framework
import 'uikit';

// For that snazzy apple tv like effect on grid cards
import "vanilla-tilt";

// Our Styles
import "./scss/master.scss";

// Imports ------------------------------------------------------------------ /

import { initProjectHandlers } from './js/projects';
import { runParticles } from './js/particles';

// Functions ---------------------------------------------------------------- /

function init() {
    initProjectHandlers();
    runParticles();
}

// Events ------------------------------------------------------------------- /

window.addEventListener('DOMContentLoaded', () => {
    init();
});