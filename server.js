const http = require('http');
const fs = require('fs');
const path = require('path');
const express = require('express');

// Since we are behind nginx
const hostname = 'localhost';
const port = 3000;

// Setup the app
const DIST_DIR = './dist'; 
const app = express();
app.use(express.static(DIST_DIR));
app.use('/static', express.static('static'));
app.use('/node_modules', express.static('node_modules'));
app.get('/', (req, res) => {
    res.sendFile( path.resolve( DIST_DIR, 'index.html'));
});

const server = http.createServer(app);
server.listen(port, hostname, () => {
    console.log(`Francis Villarba's Personal Portfolio`);
    console.log(`Server running on nginx reverse proxy http://${hostname}:${port}/`);
});